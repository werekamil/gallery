const imgArray = [
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic2007a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic1509a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic1501a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic1107a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic0715a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic1608a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/potw1345a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic1307a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic0817a.jpg",
  "https://cdn.spacetelescope.org/archives/images/wallpaper2/heic0406a.jpg",
];
const root = document.getElementById("root");

const showImage = (i) => {
  root.innerHTML = "";
  let content = `<div class='fullscreen'> <div class='selected'> <img src=${imgArray[i]}> </div> <div id='thumbnails'> </div></div>`;
  let thumbnailsContent = "";
  root.innerHTML = content;
  const thumb = document.getElementById("thumbnails");
  for (let i = 0; i < imgArray.length; i++) {
    thumbnailsContent += `<img id='img${i}' src=${imgArray[i]} alt="pht"/>`;
  }
  thumb.innerHTML += thumbnailsContent;
  thumb.innerHTML += '<i id="x" class="fa fa-times" aria-hidden="true"></i>';
  document.getElementById("x").addEventListener("click", createGallery, false);

  for (let i = 0; i < imgArray.length; i++) {
    document
      .getElementById(`img${i}`)
      .addEventListener("click", () => showImage(i), false);
  }
};

const createGallery = () => {
  root.innerHTML = "";
  let content = "";

  for (let i = 0; i < imgArray.length; i++) {
    content += `<div> <img id='img${i}' class="img" src=${imgArray[i]} alt="pht"/> </div>`;
  }

  root.innerHTML = content;

  for (let i = 0; i < imgArray.length; i++) {
    document
      .getElementById(`img${i}`)
      .addEventListener("click", () => showImage(i), false);
  }
};

createGallery();
